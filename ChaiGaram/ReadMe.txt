
*** Utilizing Menus
---------------------------------------
Please use the following steps to utilize menu style 
1. Go to Drupal Administer -> Site Building -> Menus (www.YourSite.com/?q=admin/build/menu) 
2. Edit an existing menu or create a new one. 
3. Go to Drupal Administer -> Site Building -> Blocks (www.YourSite.com/?q=admin/build/block)
4. Place menu into the "Menu" region.

NOTE: the "Menu" region can contain only a single menu, or none. 

For more information please visit http://drupal.org/node/102338 

*** Customizing the Footer
---------------------------------------
To customize the theme footer via Drupal Administer place one or multiple blocks into the "Copyright" region.
Here are sample steps to configure custom footer:
1. Go to Drupal Administer -> Site configuration -> Site information 
   (www.your-site.com/?q=admin/settings/site-information)
2. Edit the Footer message field.
3. Save your changes.
